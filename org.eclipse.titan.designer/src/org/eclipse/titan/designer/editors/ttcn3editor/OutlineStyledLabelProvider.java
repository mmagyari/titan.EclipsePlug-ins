package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.List;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;
import org.eclipse.titan.designer.AST.IOutlineElement;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.NamedBridgeScope;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.designer.editors.Stylers;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * This class represents the outline label provider that supports StyledString
 * 
 * @author Miklos Magyari
 *
 */
public final class OutlineStyledLabelProvider extends DelegatingStyledCellLabelProvider {
	public OutlineStyledLabelProvider() {
		super(new IStyledLabelProvider() {

			@Override
			public Image getImage(Object element) {
				String iconName = "titan.gif";
				if (element instanceof IOutlineElement) {
					final IOutlineElement e = (IOutlineElement) element;
					iconName = e.getOutlineIcon();
				} else if (element instanceof List<?>) {
					iconName = "imports.gif";
				}
				
				return ImageCache.getImage(iconName);
			}
			
			@Override
			public void addListener(ILabelProviderListener listener) {
				// TODO Auto-generated method stub
			}

			@Override
			public void dispose() {
				// TODO Auto-generated method stub
			}

			@Override
			public boolean isLabelProperty(Object element, String property) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void removeListener(ILabelProviderListener listener) {
				// TODO Auto-generated method stub
			}

			@Override
			public StyledString getStyledText(Object element) {
				Identifier identifier = null;
				if (element instanceof IOutlineElement) {
					final IOutlineElement e = (IOutlineElement) element;
					final String outlineText = e.getOutlineText();
					if (outlineText.length() != 0) {
						return getStyledText(element, outlineText);
					}
					identifier = e.getIdentifier();
				} else if (element instanceof List<?>) {
					return new StyledString("imports");
				}

				if (identifier == null) {
					return new StyledString("unknown");
				}
				return getStyledText(element, identifier.getDisplayName());
			}
			
			private StyledString getStyledText(Object element, String text) {
				final StyledString styledOutline = new StyledString();
				if (element instanceof Definition) {
					final Definition elemDef = (Definition)element;
					if (isInClassBody(elemDef.getMyScope())) {
						Stylers.ColoredStyler styler = null;
						switch (elemDef.getVisibilityModifier()) {
						case Private:
							styler = new Stylers.ColoredStyler(Stylers.PrivateColor);
							break;
						case Public:
							styler = new Stylers.ColoredStyler(Stylers.PublicColor);
							break;
						default:
							styler = new Stylers.ColoredStyler(Stylers.ProtectedColor);
						}
						styledOutline.append(text);
						styledOutline.append(" \u25fc", styler);
					} 
				} 
				if (styledOutline.length() == 0) {
					styledOutline.append(text);
				}
				return styledOutline;			
			}
			
			private boolean isInClassBody(Scope scope) {			
				while (scope instanceof StatementBlock || scope instanceof NamedBridgeScope) {
					scope = scope.getParentScope();
				}
				
				return scope instanceof ClassTypeBody;
			}
		});
	}
}
