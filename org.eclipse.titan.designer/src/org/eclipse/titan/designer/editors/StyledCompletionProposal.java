/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension6;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

/**
 * The StyledCompletionProposal class represents a code completion proposal
 * entry that supports styles (colors, font weight etc.) 
 * 
 * @author Miklos Magyari
 *
 */
public class StyledCompletionProposal implements ICompletionProposal, ICompletionProposalExtension6 {
	private final CompletionProposal proposal;
	private StyledString styled; 
	
	public StyledCompletionProposal(final String replacementString, final int replacementOffset, final int replacementLength, final int cursorPosition,
			final Image image, final String displayString, final IContextInformation contextInformation, final String additionalProposalInfo) {
		
		styled = new StyledString(displayString);
		proposal = new CompletionProposal(replacementString, replacementOffset, replacementLength, cursorPosition,
				image, displayString, contextInformation, additionalProposalInfo); 
	}
	
	@Override
	public StyledString getStyledDisplayString() {
		return styled;
	}

	@Override
	public void apply(IDocument document) {
		proposal.apply(document);
	}

	@Override
	public Point getSelection(IDocument document) {
		return proposal.getSelection(document);
	}

	@Override
	public String getAdditionalProposalInfo() {
		return proposal.getAdditionalProposalInfo();
	}

	@Override
	public String getDisplayString() {
		return proposal.getDisplayString();
	}

	@Override
	public Image getImage() {
		return proposal.getImage();
	}

	@Override
	public IContextInformation getContextInformation() {
		return proposal.getContextInformation();
	}
	
	/**
	 * Appends a styled string to the proposal text
	 * 
	 * @param s the string to be appended
	 * @param styler style of the string
	 */ 
	public void append(String s, Styler styler) {
		styled.append(s, styler);
	}
	
	public void append(StyledString s) {
		styled.append(s);
	}
}
